#include "fsm.h"

/**
 * implementasi finite state machine dengan software
 * @param push push dari switch, active high
 * @param output output LED, active high
 * @param state state internal FSM. belum didefinisikan
 * @param digit merupaka digit seven segment mulai dari kanan ke kiri
 */
void fsm(int pushMode, int pushSet, 
		 int *DATE_digit1, int *DATE_digit2, int *DATE_digit3, int *DATE_digit4, int *DATE_digit5, int *DATE_digit6,
		 int *TIME_digit1, int *TIME_digit2, int *TIME_digit3, int *TIME_digit4, int *TIME_digit5, int *TIME_digit6,
		 int *DP1, int *DP2, int *DP3, int *DP4, int *DP5, int *DP6,
		 int *state) {
  
  static int counter = 0;
  int Mode_Evt=0;
  int statemodeevent=15;
  int Set_Evt=0;
  int statesetevent=17;

  detectmodeevent(pushMode,&Mode_Evt,&statemodeevent);
  detectsetevent(pushSet,&Set_Evt,&statesetevent);
  
	switch (*state){
		case TIME_KEEP:
		{
			if (Mode_Evt==1){
        *state = DATE_KEEP;
        counter=0;
      } 
      if (Set_Evt==1){
        *state = TIME_SET_HOUR1;
      }
      break;
    }
		case DATE_KEEP:
		{
       if ((Mode_Evt==0)&&(Set_Evt==0)) {
        *state = DATE_KEEP;
        counter++;
         if (counter >= TIMEOUT) {
            *state = TIME_KEEP;
         }
       }
      if (Mode_Evt==1){
        *state = TIME_KEEP;
      } 
      if (Set_Evt==1){
        *state = DATE_SET_DAY1;
      }
      break;
    }
		case TIME_SET_HOUR1:
		{
      if (Mode_Evt==1){
        if (*TIME_digit6>=2){
          *TIME_digit6=0;
        }else{
          (*TIME_digit6)++;
        }
        *state = TIME_SET_HOUR1;
      } 
      if (Set_Evt==1){
        *state = TIME_SET_HOUR2;
      }
      break;
    }
		case TIME_SET_HOUR2:
		{
			if (Mode_Evt==1){
        if ((*TIME_digit5==2)&&(*TIME_digit5>=4)){
          *TIME_digit5=0;
        } else if ((*TIME_digit5!=2)&&(*TIME_digit5>=9)){
          *TIME_digit5=0;
        }else{
          (*TIME_digit5)++;
        }
        *state = TIME_SET_HOUR2;
      } 
      if (Set_Evt==1){
        *state = TIME_SET_MIN1;
      }
      break;
    }
		case TIME_SET_MIN1:
		{
			if (Mode_Evt==1){
        if (*TIME_digit4>=5){
          *TIME_digit4=0;
        } else {
          (*TIME_digit4)++;
        }
        *state = TIME_SET_MIN1;
      } 
      if (Set_Evt==1){
        *state = TIME_SET_MIN2;
      }
      break;
    }
		case TIME_SET_MIN2: 
		{
			if (Mode_Evt==1){
        if (*TIME_digit3>=9){
          *TIME_digit3=0;
        } else {
          (*TIME_digit3)++;
        }
        *state = TIME_SET_MIN2;
      } 
      if (Set_Evt==1){
        *state = TIME_SET_SEC1;
      }
      break;
    }
		case TIME_SET_SEC1:
		{
			if (Mode_Evt==1){
        if (*TIME_digit2>=6){
          *TIME_digit2=0;
        } else {
          (*TIME_digit2)++;
        }
        *state = TIME_SET_SEC1;
      } 
      if (Set_Evt==1){
        *state = TIME_SET_SEC2;
      }
      break;
    }
		case TIME_SET_SEC2:
		{
			if (Mode_Evt==1){
        if (*TIME_digit1==9){
          *TIME_digit1=0;
        } else {
          (*TIME_digit1)++;
        }
        *state = TIME_SET_SEC2;
      } 
      if (Set_Evt==1){
        *state = TIME_KEEP;
      }
      break;
    }
		case DATE_SET_DAY1:
		{
			if (Mode_Evt==1){
        if (*DATE_digit6==3){
          *DATE_digit6=0;
        } else {
          (*DATE_digit6)++;
        }
        *state = DATE_SET_DAY1;
      } 
      if (Set_Evt==1){
        *state = DATE_SET_DAY2;
      }
      break;
    }
		case DATE_SET_DAY2:
		{
      if (Mode_Evt==1){
        if (*DATE_digit5==9){
          *DATE_digit5=0;
        } else {
          (*DATE_digit5)++;
        }
        *state = DATE_SET_DAY2;
      } 
      if (Set_Evt==1){
        *state = DATE_SET_MONTH1;
      }
      break;
    }
		case DATE_SET_MONTH1:
		{
			if (Mode_Evt==1){
        if (*DATE_digit4==1){
          *DATE_digit4=0;
        } else {
          (*DATE_digit4)++;
        }
        *state = DATE_SET_MONTH1;
      } 
      if (Set_Evt==1){
        *state = DATE_SET_MONTH2;
      }
      break;
    }
		case DATE_SET_MONTH2:
		{
			if (Mode_Evt==1){
        if (*DATE_digit3==9){
          *DATE_digit3=0;
        } else {
          (*DATE_digit3)++;
        }
        *state = DATE_SET_MONTH2;
      } 
      if (Set_Evt==1){
        *state = DATE_SET_YEAR1;
      }
      break;
    }
		case DATE_SET_YEAR1:
		{
			if (Mode_Evt==1){
        if (*DATE_digit2==9){
          *DATE_digit2=0;
        } else {
          (*DATE_digit2)++;
        }
        *state = DATE_SET_YEAR1;
      } 
      if (Set_Evt==1){
        *state = DATE_SET_YEAR2;
      }
      break;
    }
		case DATE_SET_YEAR2:
		{
			if (Mode_Evt==1){
        if (*DATE_digit1==9){
          *DATE_digit1=0;
        } else {
          (*DATE_digit1)++;
        }
        *state = DATE_SET_YEAR2;
      } 
      if (Set_Evt==1){
        *state = DATE_KEEP;
      }
      break;
    }
   default:
   {

   }
	}
	
	// Perhitungan Output
  switch(*state){
	case TIME_KEEP:
	case DATE_KEEP:
	{
		*DP6=0; *DP5=0; *DP4=0; *DP3=0; *DP2=0; *DP1=0;
		break;
	}
	case TIME_SET_HOUR1:
	case DATE_SET_DAY1:
	{
		*DP6=1; *DP5=0; *DP4=0; *DP3=0; *DP2=0; *DP1=0;
		break;
	}
	case TIME_SET_HOUR2:
	case DATE_SET_DAY2:
	{
		*DP6=0; *DP5=1; *DP4=0; *DP3=0; *DP2=0; *DP1=0;
		break;
	}
	case TIME_SET_MIN1:
	case DATE_SET_MONTH1:
	{
		*DP6=0; *DP5=0; *DP4=1; *DP3=0; *DP2=0; *DP1=0;
		break;
	}
	case TIME_SET_MIN2:
	case DATE_SET_MONTH2:
	{
		*DP6=0; *DP5=0; *DP4=0; *DP3=1; *DP2=0; *DP1=0;
		break;
	}
	case TIME_SET_SEC1:
	case DATE_SET_YEAR1:
	{
		*DP6=0; *DP5=0; *DP4=0; *DP3=0; *DP2=1; *DP1=0;
		break;
	}
	case TIME_SET_SEC2:
	case DATE_SET_YEAR2:
	{
		*DP6=0; *DP5=0; *DP4=0; *DP3=0; *DP2=0; *DP1=1;
		break;
	}
	default:
  {

  }
}
}

void detectmodeevent(int pushMode,int *Mode_Evt,int *statemodeevent){
  switch (*statemodeevent){
    case MODE_OFF:
    {
      if (pushMode==1){
        *Mode_Evt=1;
        *statemodeevent = MODE_ON;
      } 
      if (pushMode==0){
        *statemodeevent = MODE_OFF;
      } 
      break;
    }
   case MODE_ON:
   {
      *Mode_Evt=0;
      if (pushMode==0){
        *statemodeevent = MODE_OFF;
      } 
      if (pushMode==1){
        *statemodeevent = MODE_ON;
      } 
      break;
   }
  }
}

void detectsetevent(int pushSet,int *Set_Evt,int *statesetevent){
  switch (*statesetevent){
    case SET_OFF:
    {
      if (pushSet==1){
        *Set_Evt=1;
        *statesetevent = SET_ON;
      }
      if (pushSet==0){
        *statesetevent = SET_OFF;
      } 
      break;
    }
   case SET_ON:
   {
      *Set_Evt=0;
      if (pushSet==0){
        *statesetevent = SET_OFF;
      } 
      if (pushSet==1){
        *statesetevent = SET_ON;
      } 
      break;
   }
  }
}
