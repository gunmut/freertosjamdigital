/*****************************************************
Chip type               : ATmega328
Program type            : Application
AVR Core Clock frequency: 16.000000 MHz
Memory model            : Small
External RAM size       : N/A
Data Stack size         : N/A
 *****************************************************/
extern "C"{
#include "fsm.h"
}
#include <Arduino_FreeRTOS.h>

//SemaphoreHandle_t xSerialSemaphore;

// constants won't change. Used here to set a pin number :
const int buttonMode = 2; //PD2
const int buttonSet = 3; //PD3
const int ledPin = 12;
const int sevenSeg6 = A0;
const int sevenSeg5 = A1;
const int sevenSeg4 = A2;
const int sevenSeg3 = A3;
const int sevenSeg2 = A4;
const int sevenSeg1 = A5;
const int segA = 5;
const int segB = 4;
const int segC = 9;
const int segD = 10;
const int segE = 11;
const int segF = 6;
const int segG = 7;
const int segDP = 8;

// Variables will change :
int hour=0;
int minute=0;
int second=0;
int day=0;
int month=0;
int year=0;
int toggle = 0;
int DP1;
int DP2;
int DP3;
int DP4;
int DP5;
int DP6;
int DATE_digit1;
int DATE_digit2;
int DATE_digit3;
int DATE_digit4;
int DATE_digit5;
int DATE_digit6;
int TIME_digit1;
int TIME_digit2;
int TIME_digit3;
int TIME_digit4;
int TIME_digit5;
int TIME_digit6;
int count = 0;
int state = TIME_KEEP;
int StartTime;
int CurrentTime;
int ElapsedTime;

void setup() {
  // put your setup code here, to run once:
  // Semaphores are useful to stop a Task proceeding, where it should be paused to wait,
  // because it is sharing a resource, such as the Serial port.
  // Semaphores should only be used whilst the scheduler is running, but we can set it up here.
//  if ( xSerialSemaphore == NULL )  // Check to confirm that the Serial Semaphore has not already been created.
//  {
//    xSerialSemaphore = xSemaphoreCreateMutex();  // Create a mutex semaphore we will use to manage the Serial Port
//    if ( ( xSerialSemaphore ) != NULL )
//      xSemaphoreGive( ( xSerialSemaphore ) );  // Make the Serial Port available for use, by "Giving" the Semaphore.
//  }

  xTaskCreate(MyTask1, "Task1", 64, NULL, 1, NULL);
  xTaskCreate(MyTask2, "Task2", 64, NULL, 2, NULL);
  xTaskCreate(MyIdleTask, "IdleTask", 64, NULL, 0, NULL);
  
  pinMode(buttonMode, INPUT_PULLUP);
  pinMode(buttonSet, INPUT_PULLUP);
  pinMode(sevenSeg6, OUTPUT);
  pinMode(sevenSeg5, OUTPUT);
  pinMode(sevenSeg4, OUTPUT);
  pinMode(sevenSeg3, OUTPUT);
  pinMode(sevenSeg2, OUTPUT);
  pinMode(sevenSeg1, OUTPUT);
  pinMode(segA, OUTPUT);
  pinMode(segB, OUTPUT);
  pinMode(segC, OUTPUT);
  pinMode(segD, OUTPUT);
  pinMode(segE, OUTPUT);
  pinMode(segF, OUTPUT);
  pinMode(segG, OUTPUT);
  pinMode(segDP, OUTPUT);
  pinMode(ledPin, OUTPUT);
}

void loop() {
}

/* Task1 with priority 1 */
static void MyTask1(void* pvParameters)
{
  int pushMode;
  int pushSet;
  while(1)
  {    
        /* Pembacaan Input dari Pushbutton active LOW */
      if (digitalRead(buttonMode)==HIGH) {
            pushMode = 0;
      } else {
            pushMode = 1;
      }
      if (digitalRead(buttonSet)==HIGH) {
            pushSet = 0;
      } else {
            pushSet = 1;
      }

      // See if we can obtain or "Take" the Serial Semaphore.
    // If the semaphore is not available, wait 5 ticks of the Scheduler to see if it becomes free.
//    if ( xSemaphoreTake( xSerialSemaphore, ( TickType_t ) 5 ) == pdTRUE )
//    {
      //inisialisasi nilai digit untuk mode  setting
      if ((state==0)||(state==1)){
        DATE_digit6=day/10;
        DATE_digit5=day%10;
        DATE_digit4=month/10;
        DATE_digit3=month%10;
        DATE_digit2=year/10;
        DATE_digit1=year%10;
        TIME_digit6=hour/10;
        TIME_digit5=hour%10;
        TIME_digit4=minute/10;
        TIME_digit3=minute%10;
        TIME_digit2=second/10;
        TIME_digit1=second%10;
      }
    
      fsm(pushMode, pushSet, &DATE_digit1, &DATE_digit2, &DATE_digit3, &DATE_digit4, &DATE_digit5, &DATE_digit6,
          &TIME_digit1, &TIME_digit2, &TIME_digit3, &TIME_digit4, &TIME_digit5, &TIME_digit6,
          &DP1, &DP2, &DP3, &DP4, &DP5, &DP6,
          &state); 
    
      //set output nilai hasil settingan
        hour=(TIME_digit6*10)+TIME_digit5;
        minute=(TIME_digit4*10)+TIME_digit3;
        second=(TIME_digit2*10)+TIME_digit1;
        day=(DATE_digit6*10)+DATE_digit5;
        month=(DATE_digit4*10)+DATE_digit3;
        year=(DATE_digit2*10)+DATE_digit1;
        
//        xSemaphoreGive( xSerialSemaphore ); // Now free or "Give" the Serial Port for others.
//    }
      vTaskDelay(100/portTICK_PERIOD_MS);
  }
}


/* Task2 with priority 2 */
static void MyTask2(void* pvParameters)
{
  boolean kabisat = false;
  while(1)
  {
    if (toggle)
        {
          digitalWrite(ledPin,LOW);
          toggle = 0;
        }
        else
        {
          digitalWrite(ledPin,HIGH);
          if ((state==0)||(state==1)){
          second++;
          }
          if(second >= 60){
            minute++;
            second=0;   
          }
          if (minute >= 60){
            hour++;
            minute = 0;
          }
          if (hour >= 24){
            hour = 0;
            day++;
          }
          if (((year%4==0)&&(year%100==0))||(year%400==0))
            kabisat = true;
          else
            kabisat = false;
            
          if (((month==1)||(month==3)||(month==5)||(month==7)||(month==8)||(month==10)||(month==12))&&(day>=31)){
            day = 1;
            month++;
          }else if (((month==4)||(month==6)||(month==9)||(month==11))&&(day>=30)){
            day = 1;
            month++;
          }else if (month==2){
            if ((kabisat==false)&&(day>=28)){
              day = 1;
            }else if ((kabisat==true)&&(day>=29)){
              day = 1;
            }
            month++;
          }
          if (month>12){
            month=1;
            year++;
          }
          if (year>99){
            year=0;
          }
          toggle = 1;
        }
        vTaskDelay(500/portTICK_PERIOD_MS);
  }
}


/* Idle Task with priority Zero */ 
static void MyIdleTask(void* pvParameters)
{
  while(1)
  {
      show(state);
  }
}

void show( int state)
{
  switch (state){
    case 0:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    {
          digitalWrite(sevenSeg6,HIGH);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(hour/10, DP6);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);
          
          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,HIGH);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(hour%10, DP5);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);
          
          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,HIGH);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(minute/10, DP4);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);  
          
          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,HIGH);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(minute%10, DP3);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);
          
          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,HIGH);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(second/10, DP2);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);
          
          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,HIGH);
          SevenSegDisplay(second%10, DP1);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);
          
      break;       
    }
    case 1:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
    case 13:
    {
          digitalWrite(sevenSeg6,HIGH);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(day/10, DP6);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);

          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,HIGH);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(day%10, DP5);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);

          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,HIGH);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(month/10, DP4);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);

          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,HIGH);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(month%10, DP3);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);

          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,HIGH);
          digitalWrite(sevenSeg1,LOW);
          SevenSegDisplay(year/10, DP2);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);

          digitalWrite(sevenSeg6,LOW);
          digitalWrite(sevenSeg5,LOW);
          digitalWrite(sevenSeg4,LOW);
          digitalWrite(sevenSeg3,LOW);
          digitalWrite(sevenSeg2,LOW);
          digitalWrite(sevenSeg1,HIGH);
          SevenSegDisplay(year%10, DP1);
          vTaskDelay(5/portTICK_PERIOD_MS);;
          SevenSegDisplay(10, 0);

      break;
    }
}
}
/* 
 *  Convert Numbers To Seven Segment Display
 *  Seven Segmnet is Common Cathode
 *  if using Anode, change HIGH to LOW
 */
void SevenSegDisplay(int digit, int dot_point){
    #define SEGMENT_ON  LOW
    #define SEGMENT_OFF HIGH
    
      switch (digit){
    
      case 0:
        digitalWrite(segA, SEGMENT_ON);
        digitalWrite(segB, SEGMENT_ON);
        digitalWrite(segC, SEGMENT_ON);
        digitalWrite(segD, SEGMENT_ON);
        digitalWrite(segE, SEGMENT_ON);
        digitalWrite(segF, SEGMENT_ON);
        digitalWrite(segG, SEGMENT_OFF);
        break;
    
      case 1:
        digitalWrite(segA, SEGMENT_OFF);
        digitalWrite(segB, SEGMENT_ON);
        digitalWrite(segC, SEGMENT_ON);
        digitalWrite(segD, SEGMENT_OFF);
        digitalWrite(segE, SEGMENT_OFF);
        digitalWrite(segF, SEGMENT_OFF);
        digitalWrite(segG, SEGMENT_OFF);
        break;
    
      case 2:
        digitalWrite(segA, SEGMENT_ON);
        digitalWrite(segB, SEGMENT_ON);
        digitalWrite(segC, SEGMENT_OFF);
        digitalWrite(segD, SEGMENT_ON);
        digitalWrite(segE, SEGMENT_ON);
        digitalWrite(segF, SEGMENT_OFF);
        digitalWrite(segG, SEGMENT_ON);
        break;
    
      case 3:
        digitalWrite(segA, SEGMENT_ON);
        digitalWrite(segB, SEGMENT_ON);
        digitalWrite(segC, SEGMENT_ON);
        digitalWrite(segD, SEGMENT_ON);
        digitalWrite(segE, SEGMENT_OFF);
        digitalWrite(segF, SEGMENT_OFF);
        digitalWrite(segG, SEGMENT_ON);
        break;
    
      case 4:
        digitalWrite(segA, SEGMENT_OFF);
        digitalWrite(segB, SEGMENT_ON);
        digitalWrite(segC, SEGMENT_ON);
        digitalWrite(segD, SEGMENT_OFF);
        digitalWrite(segE, SEGMENT_OFF);
        digitalWrite(segF, SEGMENT_ON);
        digitalWrite(segG, SEGMENT_ON);
        break;
    
      case 5:
        digitalWrite(segA, SEGMENT_ON);
        digitalWrite(segB, SEGMENT_OFF);
        digitalWrite(segC, SEGMENT_ON);
        digitalWrite(segD, SEGMENT_ON);
        digitalWrite(segE, SEGMENT_OFF);
        digitalWrite(segF, SEGMENT_ON);
        digitalWrite(segG, SEGMENT_ON);
        break;
    
      case 6:
        digitalWrite(segA, SEGMENT_ON);
        digitalWrite(segB, SEGMENT_OFF);
        digitalWrite(segC, SEGMENT_ON);
        digitalWrite(segD, SEGMENT_ON);
        digitalWrite(segE, SEGMENT_ON);
        digitalWrite(segF, SEGMENT_ON);
        digitalWrite(segG, SEGMENT_ON);
        break;
    
      case 7:
        digitalWrite(segA, SEGMENT_ON);
        digitalWrite(segB, SEGMENT_ON);
        digitalWrite(segC, SEGMENT_ON);
        digitalWrite(segD, SEGMENT_OFF);
        digitalWrite(segE, SEGMENT_OFF);
        digitalWrite(segF, SEGMENT_OFF);
        digitalWrite(segG, SEGMENT_OFF);
        break;
    
      case 8:
        digitalWrite(segA, SEGMENT_ON);
        digitalWrite(segB, SEGMENT_ON);
        digitalWrite(segC, SEGMENT_ON);
        digitalWrite(segD, SEGMENT_ON);
        digitalWrite(segE, SEGMENT_ON);
        digitalWrite(segF, SEGMENT_ON);
        digitalWrite(segG, SEGMENT_ON);
        break;
    
      case 9:
        digitalWrite(segA, SEGMENT_ON);
        digitalWrite(segB, SEGMENT_ON);
        digitalWrite(segC, SEGMENT_ON);
        digitalWrite(segD, SEGMENT_ON);
        digitalWrite(segE, SEGMENT_OFF);
        digitalWrite(segF, SEGMENT_ON);
        digitalWrite(segG, SEGMENT_ON);
        break;
    
      // all segment are ON
      case 10:
        digitalWrite(segA, SEGMENT_OFF);
        digitalWrite(segB, SEGMENT_OFF);
        digitalWrite(segC, SEGMENT_OFF);
        digitalWrite(segD, SEGMENT_OFF);
        digitalWrite(segE, SEGMENT_OFF);
        digitalWrite(segF, SEGMENT_OFF);
        digitalWrite(segG, SEGMENT_OFF);
        digitalWrite(segDP,SEGMENT_OFF);
        break;
      
      }

    if (dot_point==1){
        digitalWrite(segDP,SEGMENT_ON);
    } else if (dot_point==0){
        digitalWrite(segDP,SEGMENT_OFF);
    }
}
