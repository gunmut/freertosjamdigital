/* 
 * File:   fsm.h
 * Author: Gunawan Marbun
 *
 * Created on September 28, 2017
 */

#ifndef FSM_H
#define  FSM_H

#define TIME_KEEP 0
#define DATE_KEEP 1
#define TIME_SET_HOUR1 2
#define TIME_SET_HOUR2 3
#define TIME_SET_MIN1 4
#define TIME_SET_MIN2 5
#define TIME_SET_SEC1 6
#define TIME_SET_SEC2 7
#define DATE_SET_DAY1 8
#define DATE_SET_DAY2 9
#define DATE_SET_MONTH1 10
#define DATE_SET_MONTH2 11
#define DATE_SET_YEAR1 12
#define DATE_SET_YEAR2 13
#define MODE_OFF 15
#define MODE_ON 16
#define SET_OFF 17
#define SET_ON 18
#define TIMEOUT 30


void fsm(int pushMode, int pushSet, 
     int *DATE_digit1, int *DATE_digit2, int *DATE_digit3, int *DATE_digit4, int *DATE_digit5, int *DATE_digit6,
     int *TIME_digit1, int *TIME_digit2, int *TIME_digit3, int *TIME_digit4, int *TIME_digit5, int *TIME_digit6,
     int *DP1, int *DP2, int *DP3, int *DP4, int *DP5, int *DP6,
     int *state);
void detectmodeevent(int pushMode,int *Mode_Evt,int *statemodeeevent);
void detectsetevent(int pushSet,int *Set_Evt,int *statseteevent);
#endif  /* FSM_H */

